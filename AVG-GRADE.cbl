       IDENTIFICATION DIVISION. 
       PROGRAM-ID. AVG-GRADE.
       AUTHOR. BENJAMAS.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT GRADE-FILE ASSIGN TO "mygrade.txt"
           ORGANIZATION IS LINE SEQUENTIAL.

           SELECT RESULT-FILE ASSIGN TO "avg.txt"
           ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION.
       FD GRADE-FILE.
       01 GRADE-DETAIL.
           88 END-OF-ALL-SCORE-FILE VALUE HIGH-VALUE .
           05 ID-COURSE PIC X(6).
           05 NAME-COURSE PIC X(50).
           05 CREDIT PIC 9 VALUE ZERO.
           05 GRADE PIC X(2).

       FD RESULT-FILE.
       01 RESULT-DETAIL.
           05 AVG-GRADE PIC 9V9(2) .
           05 AVG-GRADE-SCI PIC 9V9(2) .
           05 AVG-GRADE-CS PIC 9V9(2) .
           

       WORKING-STORAGE SECTION. 
       01 GRADE-NUM PIC 9V9(2) VALUE ZERO .
       01 AVG-ALL-GRADE.
           05 ALL-SCORE PIC 9(3)V9(2) VALUE ZERO .
           05 ALL-CREDIT PIC 9(3)V9(2) VALUE ZERO .
           05 AVG-GRADE-JA PIC 9V9(2) VALUE ZERO .

       01 AVG-SCI-GRADE.
           05 ALL-SCORE-SCI PIC 9(3)V9(2) VALUE ZERO .
           05 ALL-CREDIT-SCI PIC 9(3)V9(2) VALUE ZERO .
           05 AVG-GRADE-SCI PIC 9V9(2) VALUE ZERO .

       01 AVG-CS-GRADE.
           05 ALL-SCORE-CS PIC 9(3)V9(2) VALUE ZERO .
           05 ALL-CREDIT-CS PIC 9(3)V9(2) VALUE ZERO .
           05 AVG-GRADE-CS PIC 9V9(2) VALUE ZERO .

       01 TWO-DIGIT-ID-COURSE PIC XX.
       01 FIRSE-DIGIT-ID-COURSE PIC X.


       PROCEDURE DIVISION .
       BEGIN.
           OPEN INPUT GRADE-FILE
           OPEN OUTPUT RESULT-FILE 

           PERFORM UNTIL END-OF-ALL-SCORE-FILE 
              READ GRADE-FILE AT END SET END-OF-ALL-SCORE-FILE TO TRUE  
              END-READ
              IF NOT END-OF-ALL-SCORE-FILE THEN
                 PERFORM 001-PROCESS THRU 001-EXIT
              END-IF 
           END-PERFORM 

              DISPLAY "SUM GRADE  " ALL-SCORE 
              DISPLAY "SUM-CREDIT " ALL-CREDIT
              COMPUTE AVG-GRADE-JA = ALL-SCORE / ALL-CREDIT 
              DISPLAY "AVG-GRADE  " AVG-GRADE-JA

              DISPLAY " "

              DISPLAY "SUM GRADE-SCI  " ALL-SCORE-SCI  
              DISPLAY "SUM-CREDIT-SCI " ALL-CREDIT-SCI 
              COMPUTE AVG-GRADE-SCI IN AVG-SCI-GRADE 
                                      = ALL-SCORE-SCI / ALL-CREDIT-SCI 
              DISPLAY "AVG-GRADE-SCI  " AVG-GRADE-SCI IN AVG-SCI-GRADE 

              DISPLAY " "

              DISPLAY "SUM GRADE-CS  " ALL-SCORE-CS
              DISPLAY "SUM-CREDIT-CS " ALL-CREDIT-CS
              COMPUTE AVG-GRADE-CS  IN AVG-CS-GRADE
                                   = ALL-SCORE-CS / ALL-CREDIT-CS 
              DISPLAY "AVG-GRADE-CS  " AVG-GRADE-CS IN AVG-CS-GRADE

              MOVE AVG-GRADE-JA IN AVG-ALL-GRADE 
                    TO AVG-GRADE IN RESULT-DETAIL 
               

              MOVE AVG-GRADE-SCI IN AVG-SCI-GRADE 
                    TO AVG-GRADE-SCI IN RESULT-DETAIL 
               

              MOVE AVG-GRADE-CS  IN AVG-CS-GRADE 
                    TO AVG-GRADE-CS  IN RESULT-DETAIL  
              WRITE RESULT-DETAIL 
           
           CLOSE GRADE-FILE 
           CLOSE RESULT-FILE 
           
           GOBACK.

       001-PROCESS.
            
           EVALUATE TRUE 
              WHEN GRADE = "A" MOVE 4.00 TO GRADE-NUM
              WHEN GRADE = "B+" MOVE 3.50 TO GRADE-NUM
              WHEN GRADE = "B" MOVE 3.00 TO GRADE-NUM
              WHEN GRADE = "C+" MOVE 2.50 TO GRADE-NUM
              WHEN GRADE = "C" MOVE 2.00 TO GRADE-NUM
              WHEN GRADE = "D+" MOVE 1.50 TO GRADE-NUM
              WHEN GRADE = "D" MOVE 1.00 TO GRADE-NUM
           END-EVALUATE

           COMPUTE ALL-SCORE = (CREDIT * GRADE-NUM) + ALL-SCORE  
           COMPUTE ALL-CREDIT = CREDIT + ALL-CREDIT 

           MOVE ID-COURSE IN GRADE-DETAIL TO FIRSE-DIGIT-ID-COURSE 
           MOVE ID-COURSE IN GRADE-DETAIL TO TWO-DIGIT-ID-COURSE 

           IF FIRSE-DIGIT-ID-COURSE  = "3" THEN
              COMPUTE ALL-SCORE-SCI  = (CREDIT * GRADE-NUM) 
                                         + ALL-SCORE-SCI   
              COMPUTE ALL-CREDIT-SCI  = CREDIT + ALL-CREDIT-SCI

              IF TWO-DIGIT-ID-COURSE = "31" THEN
                 COMPUTE ALL-SCORE-CS  = (CREDIT * GRADE-NUM) 
                                            + ALL-SCORE-CS  
                 COMPUTE ALL-CREDIT-CS  = CREDIT + ALL-CREDIT-CS  
              END-IF 
               
           END-IF 

           .

       001-EXIT .
           EXIT.


